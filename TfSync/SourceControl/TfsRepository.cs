﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using TfSync.Changes;
using TfSync.Helpers;
using ChangeType = TfSync.Changes.ChangeType;
using TfsChangeType = Microsoft.TeamFoundation.VersionControl.Client.ChangeType;

namespace TfSync.SourceControl
{
	public class TfsRepository
	{
		private static readonly HashSet<string> _ignoreList = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
												    {
													 ".vspscc",
															   ".vssscc"
												 };
		private static readonly Dictionary<TfsChangeType, Changes.ChangeType> _changeTypes = 
			new Dictionary<TfsChangeType, Changes.ChangeType>
			{
				{TfsChangeType.Add, Changes.ChangeType.Add},
				{TfsChangeType.Delete, Changes.ChangeType.Remove},
				{TfsChangeType.Undelete, Changes.ChangeType.Add},
				{TfsChangeType.Edit, Changes.ChangeType.Modify},
				{TfsChangeType.Encoding, Changes.ChangeType.Modify},
				{TfsChangeType.Merge, Changes.ChangeType.Modify},
				{TfsChangeType.Rename, Changes.ChangeType.Add}
			};

		private readonly Workspace _workspace;
		private readonly string _path;
		private readonly WorkItemStore _workItemStore;

		public TfsRepository(string path)
		{
			_path = path;
			_workspace = FindWorkspace(path);
		  if (null == this._workspace)
			 throw new InvalidOperationException(string.Format("Cannot find a workspace bound to the TFS master directory {0}. Verify that the team project is registered on that machine and user.", path) );
			_workItemStore = new WorkItemStore(_workspace.VersionControlServer.TeamProjectCollection);
		  if (null == this._workItemStore)
				throw new InvalidOperationException(string.Format("Cannot find the workspace storage for the TFS master directory {0}.", path));
		}

		public bool HasPendingChanges
		{
			get
			{
				return _workspace.GetPendingChanges().Any();
			}
		}

		public int LastChangeSetId
		{
			get
			{
				return _workspace.VersionControlServer.QueryHistory(_path, RecursionType.Full, 1).First().ChangesetId;
			}
		}

		public HashSet<string> GetItemsUnderControl(bool relative = false)
		{
			var rootItem = _workspace.VersionControlServer.GetItem(_path);

			var result = _workspace.VersionControlServer.GetItems(_path, RecursionType.Full).Items
				.Where(x => x.ItemType == ItemType.File && !_ignoreList.Any(s => Path.GetFileName(x.ServerItem).EndsWith(s)))
				.Select(x => PathHelpers.RebasePath(x.ServerItem, rootItem.ServerItem, _path));

			if (relative)
				result = result.Select(x => PathHelpers.MakeRelative(x, _path));

			return result.ToCaseInsensitiveHashSet();
		}

		public void Update()
		{
			_workspace.Get(new GetRequest(_path, RecursionType.Full, VersionSpec.Latest), GetOptions.GetAll);
		}

		public void PropagateChanges(List<FileChange> changes, HashSet<string> hgControlled, string hgBase)
		{
			var directories = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

			foreach (var change in changes)
			{
				if (change.Type == Changes.ChangeType.Add)
					_workspace.PendAdd(change.Path);
				else if (change.Type == Changes.ChangeType.Modify)
					_workspace.PendEdit(change.Path);
				else if (change.Type == Changes.ChangeType.Remove)
				{
					_workspace.PendDelete(change.Path);
					directories.Add(Path.GetDirectoryName(change.Path));
				}
			}

			foreach (var directory in directories)
			{
				if (!Directory.Exists(directory))
					continue;

				var dirs = Directory.GetDirectories(directory).Select(x => PathHelpers.MakeRelative(x, _path));
				foreach (var dir in dirs)
				{
					if (hgControlled.Contains(dir))
						goto Freedom;
				}

				var files = Directory.GetFiles(directory).Select(x => PathHelpers.MakeRelative(x, _path));
				foreach (var file in files)
				{
					if (hgControlled.Contains(file))
						goto Freedom;
				}
				
				_workspace.PendDelete(directory);

			Freedom:
				;
			}
		}

		public int CheckIn(string comment, int[] workItems)
		{
			var parameters = new WorkspaceCheckInParameters(_workspace.GetPendingChanges(), comment);
			if (workItems != null && workItems.Length > 0)
				parameters.AssociatedWorkItems = workItems.Select(x => new WorkItemCheckinInfo(_workItemStore.GetWorkItem(x), WorkItemCheckinAction.Associate)).ToArray();

			_workspace.CheckIn(parameters);

			return _workspace.VersionControlServer.GetLatestChangesetId();
		}

		public void Rollback()
		{
			_workspace.Undo(_workspace.GetPendingChanges());
		}

		public TfsChanges GetChanges(int lastPulledChangeset)
		{
			var changesets = _workspace.VersionControlServer.QueryHistory(_path, RecursionType.Full);
			var changes = new List<Change>();
			int changesetFrom = 0;
			int changesetTo = changesets.First().ChangesetId;
			foreach (var changeset in changesets)
			{
				if (changeset.ChangesetId <= lastPulledChangeset)
					break;

				changesetFrom = changeset.ChangesetId;
				changes.AddRange(_workspace.VersionControlServer.GetChangesForChangeset(changeset.ChangesetId).Where(ch => ch.Item.ItemType == ItemType.File));
			}

			if (changes.Count == 0)
				return null;

			var changesDistinct = new Dictionary<string, Changes.ChangeType>();
			for (int i = changes.Count - 1; i >= 0; i--)
			{
				var change = changes[i];
				changesDistinct[change.Item.ServerItem] = ToChangeType(change.ChangeType);
			}

			return new TfsChanges
				  {
					   Changes = changesDistinct.Select(x => new FileChange(_workspace.GetLocalItemForServerItem(x.Key), x.Value)).ToList(),
					   ChangesetFrom = changesetFrom,
					   ChangeSetTo = changesetTo
				  };
		}

		private static Changes.ChangeType ToChangeType(TfsChangeType changeType)
		{
			foreach (var type in _changeTypes)
			{
				if (changeType.HasFlag(type.Key))
					return type.Value;
			}

			throw new InvalidOperationException("The TFS master has unsupported change type.");
		}

		private static Workspace FindWorkspace(string path)
		{
			var collections = RegisteredTfsConnections.GetProjectCollections();
			if (collections == null || collections.Length == 0)
				return null;

			foreach (var projectCollection in collections)
			{
				var collection = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(projectCollection);
				if (collection == null)
					continue;

				var service = collection.GetService<VersionControlServer>();
				var workspace = service.TryGetWorkspace(path);
				if (workspace != null)
					return workspace;
			}

			return null;
		}
	}
}