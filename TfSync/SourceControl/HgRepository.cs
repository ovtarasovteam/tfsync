﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Mercurial;
using TfSync.Changes;
using TfSync.Helpers;
using TfSync.Properties;
using TfSync.Settings;

namespace TfSync.SourceControl
{
	/// <summary>
	/// A wrapper for Mercurial library.
	/// </summary>
	public class HgRepository
	{
		private const string SourceTreeIdentifier = "{SOURCETREE_LOCAL}";
		private const string RepoSettings = "tfsync.xml";

		private static readonly HashSet<string> _ignoreList = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		                                                      {
			                                                   RepoSettings,
															   ".hgignore",
															   FileHelper.TfsSectionExtension
		                                                   };

		private static readonly Dictionary<ChangesetPathActionType, ChangeType> _changeTypes =
			new Dictionary<ChangesetPathActionType, ChangeType>
			{
				{ChangesetPathActionType.Add, ChangeType.Add},
				{ChangesetPathActionType.Modify, ChangeType.Modify},
				{ChangesetPathActionType.Remove, ChangeType.Remove}
			};

		private readonly Repository _repository;
		
		private SettingsRoot _settings;

		private string SettingsPath {get { return Path.Combine(_repository.Path, RepoSettings); }}

		private SettingsRoot Settings
		{
			get
			{
				if (_settings == null)
					throw new InvalidOperationException("Repository is not managed by tfsync!");

				return _settings;
			}
			set { _settings = value; }
		}

		/// <summary>
		/// Full path to TFS master repository.
		/// </summary>
		public string MasterPath
		{
			get
			{
				return Path.IsPathRooted(Settings.MasterPath)
					? Settings.MasterPath
					: PathHelpers.MakeAbsolute(Settings.MasterPath, _repository.Path);
			}
		}

		/// <summary>
		/// Indicates whether Mercurial repo has uncommited changes.
		/// </summary>
		public bool HasUncommitedChanges
		{
			get
			{
				return _repository.Status().Any();
			}
		}

		/// <summary>
		/// Returns the number of last synchronized revision.
		/// </summary>
		public int LastRevision { get { return Settings.LastRevision; } }

		/// <summary>
		/// Returns the last TFS changeset that was synchronized with Mercurial repo.
		/// </summary>
		public int LastMasterChangeset { get { return Settings.LastMasterChangeset; }}

		/// <summary>
		/// Returns current Mercurial revision number.
		/// </summary>
		public int CurrentRevision {get { return GetCurrentRevision().RevisionNumber; }}

		/// <summary>
		/// Returns Mercurial repository path.
		/// </summary>
		public string RepositoryPath { get { return _repository.Path; } }

		static HgRepository()
		{
			SetupClient();
		}

		private HgRepository(string path, SettingsRoot settings = null)
		{
			_settings = settings;
			_repository = new Repository(path);
		}

		/// <summary>
		/// Checks whether we can clone a repo into specified directory.
		/// </summary>
		/// <param name="path">Directory to clone to</param>
		public static void VerifyCloneTarget(string path)
		{
			if (!Directory.Exists(path))
				return;

			try
			{
				var tmp = new Repository(path);
				var log = tmp.Log().ToList();
				if (log.Count != 0)
					throw new InvalidOperationException("Can't clone into a repository with existing revisions!");
			}
			catch (MercurialExecutionException)
			{
			}
			
			if (Directory.GetFiles(path).Length > 0)
				throw new InvalidOperationException("Can't clone into a non-empty directory!");

			var dirs = Directory.GetDirectories(path);
			if (dirs.Length > 1 || (dirs.Length == 1 && !dirs[0].EndsWith("\\.hg", StringComparison.OrdinalIgnoreCase)))
				throw new InvalidOperationException("Can't clone into a non-empty directory!");
		}

		/// <summary>
		/// Links existing Mercurial and TFS repositories.
		/// </summary>
		/// <param name="path">Path to a Mercurial repo</param>
		/// <param name="master">Path to a TFS repo</param>
		/// <param name="masterChangeset">Current TFS changeset number</param>
		public void Link(string path, string master, int masterChangeset)
		{
			if (HasUncommitedChanges)
				throw new InvalidOperationException("Cannot link while the Hg slave repository has uncommited changes.");

			Settings = new SettingsRoot
			{
				MasterPath = master,
				LastMasterChangeset = masterChangeset
			};

			PathHelpers.IterateDirectories(path, null, (absolutePath, relativePath) =>
			{
				var file = new FileInfo(absolutePath);
				FileHelper.TransformFile(file, FileHelper.CopyDirection.MasterToSlave, Path.GetDirectoryName(absolutePath));
			});

			_repository.AddRemove();
			if (HasUncommitedChanges)
				_repository.Commit("Linked with TFS repository");

			Settings.LastRevision = GetCurrentRevision().RevisionNumber;
			SaveSettings();
		}

		/// <summary>
		/// Opens a Mercurial repo from the specified path.
		/// </summary>
		public static HgRepository Open(string path)
		{
			string settingsPath = Path.Combine(path, RepoSettings);
			SettingsRoot settigns = null;
			if (File.Exists(settingsPath))
				settigns = SettingsRoot.ReadSettings(settingsPath);

			return new HgRepository(path, settigns);
		}
	   
		/// <summary>
		/// Creates a new Mercurial repository on the specified path.
		/// </summary>
		public static HgRepository Create(string path, string master, int masterChangeset)
		{
			var repository = new HgRepository(path, new SettingsRoot
			                                        {
				                                        MasterPath = master, 
														LastMasterChangeset = masterChangeset
			                                        });
			repository.InitializeClone();

			return repository;
		}

		/// <summary>
		/// Gets TFS push comment when no custom comment was specified.
		/// </summary>
		public string GetDefaultComment()
		{
			var revTo = GetCurrentRevision();

			if (Settings.LastRevision == revTo.RevisionNumber)
				return null;

			var revFrom = GetFirstRevisionToPush();
			var revisions = _repository.Log(revFrom.RevisionNumber + ":" + revTo.RevisionNumber);
			var result = new StringBuilder();

			result.AppendLine(GetDefaultCommentHeader(revFrom, revTo));
			foreach (var rev in revisions)
				result.Append("* ").AppendLine(rev.CommitMessage);

			return result.ToString();
		}

		/// <summary>
		/// Gets TFS push comment when no changeset messages where specified.
		/// </summary>
		private string GetDefaultCommentHeader(Changeset revFrom, Changeset revTo)
		{
			string result = "[Pushed revision";
			if (revFrom.RevisionNumber == revTo.RevisionNumber)
				result += " " + revFrom.RevisionNumber.ToString();
			else
				result += "s " + revFrom.RevisionNumber.ToString() + "-" + revTo.RevisionNumber.ToString();

			return result + " from Mercurial slave repository]";
		}

		/// <summary>
		/// Gets a list of file changes since last sync.
		/// </summary>
		public List<FileChange> GetChangesToPush()
		{
			var revTo = GetCurrentRevision();

			if (Settings.LastRevision == revTo.RevisionNumber)
				return null;

			var revFrom = GetFirstRevisionToPush();
			bool reverse = revTo.RevisionNumber < revFrom.RevisionNumber;
			var revisions = _repository.Log(revFrom.RevisionNumber + ":" + revTo.RevisionNumber);
			var changes = new Dictionary<string, ChangeType>();

			foreach (var changeset in revisions)
			{
				foreach (var change in changeset.PathActions)
					ProcessChange(changes, change, reverse);
			}

			return changes.Select(x => new FileChange(x.Key, x.Value)).ToList();
		}

		/// <summary>
		/// Processes single file change in context of all other changes.
		/// </summary>
		/// <param name="changes">All changes so far</param>
		/// <param name="change">Single file change</param>
		/// <param name="reverse">Is operation a reverse one</param>
		private void ProcessChange(Dictionary<string, ChangeType> changes, ChangesetPathAction change, bool reverse)
		{
			var newType = ToChangeType(change.Action, reverse);
			if (!changes.ContainsKey(change.Path))
			{
				changes[change.Path] = newType;
				return;
			}

			var existingType = changes[change.Path];
			if (existingType == ChangeType.Add)
			{
				if (newType == ChangeType.Remove)
					changes.Remove(change.Path);
			}
			else if (existingType == ChangeType.Modify)
			{
				if (newType == ChangeType.Add)
				   throw new InvalidOperationException("Found an invalid change state sequence.");
				
				changes[change.Path] = newType;
			}
			else if (existingType == ChangeType.Remove)
			{
				if (newType == ChangeType.Modify)
					throw new InvalidOperationException("Found an invalid change state sequence.");

				changes[change.Path] = newType;
			}
		}

		/// <summary>
		/// Saves cuurent HG and TFS revisions to settings.
		/// </summary>
		/// <param name="changesetId">Latest TFS revision after push</param>
		public void ConfirmPush(int changesetId)
		{
			Settings.LastRevision = GetCurrentRevision().RevisionNumber;
			Settings.LastMasterChangeset = changesetId;
			SaveSettings();
		}

		/// <summary>
		/// Returns the list of files under source control.
		/// </summary>
		public HashSet<string> GetFilesUnderControl(bool relative = false)
		{
			var result = _repository
				.Status(new StatusCommand {Include = FileStatusIncludes.Clean})
				.Select(x => x.Path)
				.Where(x => !_ignoreList.Any(s => Path.GetFileName(x).EndsWith(s)));

			if (!relative)
				result = result.Select(x => PathHelpers.MakeAbsolute(x, RepositoryPath));
			
			return result.ToCaseInsensitiveHashSet();
		}

		/// <summary>
		/// Commits changes pulled from TFS master.
		/// </summary>
		public void CommitPulled(int tfsFrom, int tfsTo)
		{
			_repository.AddRemove();
			_repository.Commit("Pulled TFS master changesets " + FormatTfsRange(tfsFrom, tfsTo));

			Settings.LastRevision = CurrentRevision;
			Settings.LastMasterChangeset = tfsTo;
			SaveSettings();
		}

		/// <summary>
		/// Merges changesets pulled from TFS.
		/// </summary>
		/// <param name="mergeRevision">Revision that was current before we started the pull</param>
		/// <param name="tfsFrom">TFS start revision</param>
		/// <param name="tfsTo">TFS end revision</param>
		public void MergePulled(int mergeRevision, int tfsFrom, int tfsTo)
		{
			CommitPulled(tfsFrom, tfsTo);

			int mergeSource = CurrentRevision;
			
			_repository.Update(mergeRevision);
			_repository.Merge(mergeSource);
			_repository.Commit("Merged TFS master with Hg slave changes.");
			Settings.LastRevision = CurrentRevision;
			SaveSettings();
		}

		/// <summary>
		/// Updates repository to the last revision which was synchronized with TFS.
		/// </summary>
		/// <returns>Current revision number.</returns>
		public int UpdateToLastRevision()
		{
			int tip = CurrentRevision;
			_repository.Update(new UpdateCommand { Revision = Settings.LastRevision, Clean = true });
			
			return tip;
		}

		/// <summary>
		/// Formats TFS changeset range.
		/// </summary>
		private string FormatTfsRange(int tfsFrom, int tfsTo)
		{
			return tfsFrom == tfsTo ? tfsFrom.ToString() : tfsFrom.ToString() + "-" + tfsTo.ToString();
		}

		/// <summary>
		/// Converts Mercurial change type with respect to sync direction.
		/// </summary>
		/// <param name="actionType">Mercurial action type</param>
		/// <param name="reverse">Is this sync a reverse one</param>
		/// <returns>Adjusted change type</returns>
		private ChangeType ToChangeType(ChangesetPathActionType actionType, bool reverse)
		{
			var result = _changeTypes[actionType];
			if (result == ChangeType.Modify || !reverse)
				return result;

			return result == ChangeType.Add ? ChangeType.Remove : ChangeType.Add;
		}

		/// <summary>
		/// Finds a first revision to push to TFS.
		/// </summary>
		/// <returns>First revision to push from.</returns>
		private Changeset GetFirstRevisionToPush()
		{
			var revisions = _repository.Log(CurrentRevision + ":" + Settings.LastRevision).ToList();
			if (revisions.Count < 2)
				throw new InvalidOperationException("Working copy in Hg slave does not have enough revisions to push.");
			
			return _repository.Revision(revisions[revisions.Count - 2].RevisionNumber);
		}

		/// <summary>
		/// Gets current Mercurial repo revision .
		/// </summary>
		private Changeset GetCurrentRevision()
		{
			var currentRevisions = _repository.Summary().ParentRevisionNumbers.ToList();
			if (currentRevisions.Count != 1)
				throw new InvalidOperationException("Working copy in Hg slave has more than one parent revision!");

			return _repository.Revision(currentRevisions[0]);
		}

		/// <summary>
		/// Generates a Mercurial repository and commits all files within the directory.
		/// </summary>
  		private void InitializeClone()
		{
			try
			{
				_repository.Log();
			}
			catch (MercurialExecutionException)
			{
				_repository.Init();
			}

			WriteHgIgnore();
			_repository.AddRemove();
			_repository.Commit(String.Format("Clone created from TFS change-set #{0}.", LastMasterChangeset));
			Settings.LastRevision = CurrentRevision;
			SaveSettings();
		}

		/// <summary>
		/// Writes the default hgignore for a new repo.
		/// </summary>
		private void WriteHgIgnore()
		{
			using (var writer = new StreamWriter(Path.Combine(_repository.Path, ".hgignore")))
			{
				writer.Write(Resources.hgignore);
			}
		}

		/// <summary>
		/// Configures Mercurial library.
		/// </summary>
		private static void SetupClient()
		{
			string clientPath = ConfigurationManager.AppSettings["HgHome"];
			if (!String.IsNullOrEmpty(clientPath))
			{
				if (clientPath == SourceTreeIdentifier)
					clientPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Atlassian\SourceTree\hg_local");

				if (Directory.Exists(clientPath))
					ClientExecutable.SetClientPath(clientPath);
			}
		}

		/// <summary>
		/// Saves current settigns.
		/// </summary>
		private void SaveSettings()
		{
			Settings.WriteSettings(SettingsPath);
		}
	}
}