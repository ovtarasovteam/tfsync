﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
namespace TfSync
{
	/// <summary>
	///      The collection of possible exit statuses.
	/// </summary>
	public enum ExitCodes : int
	{
		/// <summary>
		///      The application encountered an error which is not known.
		/// </summary>
		Unknown = -1,

		/// <summary>
		///      The application successfully executed the requested command
		///      correctly and without unrecoverable errors.
		/// </summary>
		Success = 0,

		/// <summary>
		///      The user failed to provide a command to execute.
		/// </summary>
		CommandMissing = 1,

		/// <summary>
		///      The user provided a command which is not known.
		/// </summary>
		CommandUnknown = 2,

		/// <summary>
		///      The user failed to provide any arguments for a command to
		///      execute.
		/// </summary>
		CommandArgumentMissing = 3,

		/// <summary>
		///      The user failed to provide an argument which is required for
		///      the specified command.
		/// </summary>
		CommandArgumentRequiredMissing = 4,

		/// <summary>
		///      The command encountered during execution an unrecoverable error
		///      for a known reason.
		/// </summary>
		CommandExecutionFailure = 5,

		/// <summary>
		///      There is nothing to pull from master repository.
		/// </summary>
		NothingToPull = 6,

		/// <summary>
		///      There is nothing to push to master repository.
		/// </summary>
		NothingToPush = 7,

		/// <summary>
        ///      The version of TFS that TfSync was built with could not be
		///      loaded.
		/// </summary>
		MissingTFS = 8
	}
}