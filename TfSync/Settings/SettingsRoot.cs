﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using TfSync.Transformers;

namespace TfSync.Settings
{
	[Serializable]
	public class SettingsRoot
	{
		public string MasterPath { get; set; }
		public int LastRevision { get; set; }
		public int LastMasterChangeset { get; set; }
		
		public void WriteSettings(string path)
		{
			using (var stream = new StreamWriter(path))
			{
				var serializer = new XmlSerializer(typeof(SettingsRoot));
				serializer.Serialize(stream, this);
			}
		}

		public static SettingsRoot ReadSettings(string path)
		{
			using (var reader = XmlReader.Create(path))
			{
				return (SettingsRoot)new XmlSerializer(typeof(SettingsRoot)).Deserialize(reader);
			}
		}
	}
}