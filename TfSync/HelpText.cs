﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;

namespace TfSync
{
	public static class HelpText
	{
		public const string CloneDescription = "Create an Hg slave repository from a TFS working directory.";
		public const string CloneRepoDecription = "Full path to Hg slave repository.";
		public const string CloneMasterDescription = "Full path to TFS master working copy.";

		public const string PullDescription = "Get changes from TFS master and place into Hg slave.";

		public const string PushDescription = "Put Hg slave changes into TFS master.";
		public const string PushCommentDecription = "A comment for the TFS master check-in.";
		public const string PushWorkitemsDescription = "Associate work item id in TFS master check-in.";

		public const string ExposeDescription = "Make all read-only files in repository as writable.";

		public const string LinkDescription = "Link existing TFS and Mercurial repositories.";
		public const string PriorityDescription = "Link priority. If not set, assumes that repositories are equal. If set to Master or Slave, it will forcibly synchronize repositories.";
	}
}