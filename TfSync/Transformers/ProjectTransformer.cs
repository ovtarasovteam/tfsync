﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TfSync.Helpers;
using JetBrains.Annotations;

namespace TfSync.Transformers
{
	public class ProjectTransformer : ITransformer
	{
		public StripTfsInfoResult StripTfsInfo([NotNull] string path)
		{
			if (path == null)
				throw new ArgumentNullException("path");

			var doc = XDocument.Load(path);
			if (doc.Root == null)
				throw new InvalidDataException("No root node in project!");

			var sections = new ProjectTfsSections();

			int cnt = 0;
			foreach (var element in doc.Root.ChildElements("PropertyGroup"))
			{
				var section = ReadTfsSection(element);
				if (section != null)
				{
					section.SectionIndex = cnt;
					RemoveSccNodes(element);
					sections.Sections.Add(section);
				}

				cnt++;
			}

			return new StripTfsInfoResult
			       {
				       Content = doc.ToString(),
					   TfsSection = sections.Sections.Count == 0 ? null : SerializeSections(sections)
			       };
		}

		public string AppendTfsInfo(string path, string sectionPath)
		{
			var doc = XDocument.Load(path);
			if (doc.Root == null)
				throw new InvalidDataException("No root node in project!");

			var sections = DeserializeSections(sectionPath);
			if (sections.Sections == null || sections.Sections.Count == 0)
				return doc.ToString();

			var groups = doc.Root.ChildElements("PropertyGroup").ToList();
			foreach (var section in sections.Sections)
			{
				var group = section.SectionIndex >= groups.Count ? groups[groups.Count - 1] : groups[section.SectionIndex];
				
				AddSccNode(group, "SccAuxPath", section.SccAuxPath);
				AddSccNode(group, "SccLocalPath", section.SccLocalPath);
				AddSccNode(group, "SccProjectName", section.SccProjectName);
				AddSccNode(group, "SccProvider", section.SccProvider);
			}

			return doc.ToString();
		}

		private ProjectTfsSections DeserializeSections(string path)
		{
			using (var reader = XmlReader.Create(path))
			{
				return (ProjectTfsSections)new XmlSerializer(typeof(ProjectTfsSections)).Deserialize(reader);
			}
		}

		private string SerializeSections(ProjectTfsSections sections)
		{
			using (var stream = new StringWriter())
			using (var writer = XmlWriter.Create(stream, new XmlWriterSettings {OmitXmlDeclaration = true}))
			{
				var serializer = new XmlSerializer(typeof(ProjectTfsSections));
				serializer.Serialize(writer, sections);
				return stream.ToString();
			}
		}

		private void AddSccNode(XElement group, string name, string value)
		{
			if (string.IsNullOrEmpty(value))
				return;

			var element = group.ChildElement(name);
			if (element != null)
				element.Value = value;
			else
				group.AddElement(new XElement(name, value));
		}

		private ProjectTfsSection ReadTfsSection(XElement group)
		{
			var section = new ProjectTfsSection
			              {
				              SccAuxPath = GetChildNodeValue(group, "SccAuxPath"),
				              SccLocalPath = GetChildNodeValue(group, "SccLocalPath"),
				              SccProjectName = GetChildNodeValue(group, "SccProjectName"),
				              SccProvider = GetChildNodeValue(group, "SccProvider")
			              };

			if (string.IsNullOrEmpty(section.SccAuxPath)
			    && string.IsNullOrEmpty(section.SccLocalPath)
			    && string.IsNullOrEmpty(section.SccProjectName)
			    && string.IsNullOrEmpty(section.SccProvider))
			{
				return null;
			}

			return section;
		}

		private void RemoveSccNodes(XElement group)
		{
			RemoveNode(group, "SccAuxPath");
			RemoveNode(group, "SccLocalPath");
			RemoveNode(group, "SccProjectName");
			RemoveNode(group, "SccProvider");
		}

		private void RemoveNode(XElement element, string name)
		{
			var node = element.ChildElement(name);
			if (node != null)
				node.Remove();
		}

		private string GetChildNodeValue(XElement element, string name)
		{
			var node = element.ChildElement(name);
			if (node == null || string.IsNullOrEmpty(node.Value))
				return null;

			return node.Value;
		}
	}
}