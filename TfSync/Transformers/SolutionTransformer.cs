﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace TfSync.Transformers
{
	public class SolutionTransformer : ITransformer
	{
		private const string TfsSection = "GlobalSection(TeamFoundationVersionControl)";
		private const string EndSection = "EndGlobalSection";
		private const string GlobalSection = "Global";

		public StripTfsInfoResult StripTfsInfo(string path)
		{
			bool skipping = false;
			var solution = new StringBuilder();
			var section = new StringBuilder();
			foreach (var line in File.ReadAllLines(path))
			{
				string trimmed = line.Trim();
				if (trimmed.StartsWith(TfsSection))
					skipping = true;

				if (!skipping)
					solution.AppendLine(line);
				else
					section.AppendLine(line);

				if (trimmed == EndSection && skipping)
					skipping = false;
			}

			var result = new StripTfsInfoResult
			             {
				             Content = solution.ToString(),
							 TfsSection = section.ToString()
			             };
			

			return result;
		}

		public string AppendTfsInfo(string path, string sectionPath)
		{
			var solution = new StringBuilder();
			bool appended = false;
			foreach (var line in File.ReadAllLines(path))
			{
				solution.AppendLine(line);

				if (!appended && line.Trim() == GlobalSection)
				{
					solution.AppendLine(File.ReadAllText(sectionPath));
					appended = true;
				}
			}

			return solution.ToString();
		}
	}
}