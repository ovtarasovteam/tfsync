﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace TfSync.Helpers
{
	public static class PathHelpers
	{
		private static readonly char[] _separators = new[] {Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar};

        public static int GetPathHashCode(string path)
        {
            return path.Trim(_separators).ToUpperInvariant().GetHashCode();
        }

        public static bool PathEquals(string path1, string path2)
        {
            var parts1 = ExplodePath(path1);
            var parts2 = ExplodePath(path2);

            if (parts1.Length != parts2.Length)
                return false;

            for (int i = 0; i < parts1.Length; i++)
            {
                if (!string.Equals(parts1[i], parts2[i], StringComparison.OrdinalIgnoreCase))
                    return false;
            }

            return true;
        }

		public delegate bool DirectoryProcessor(string absolutePath, string relativePath);
		public delegate void FileProcessor(string absolutePath, string relativePath);

		public static void IterateDirectories(string root, DirectoryProcessor dirAction, FileProcessor fileAction, char directorySeparator = '\\')
		{
			var queue = new Queue<string>();
			queue.Enqueue(root);

			while (queue.Count > 0)
			{
				string dir = queue.Dequeue();
				if (directorySeparator != Path.DirectorySeparatorChar)
					dir = dir.Replace(Path.DirectorySeparatorChar, directorySeparator);
				dir = EnsureSlash(dir, directorySeparator);

				string relative = EnsureSlash(MakeRelative(dir, root, directorySeparator), directorySeparator);
				if (dirAction != null && !dirAction(dir, relative))
					continue;

				if (fileAction != null)
				{
					foreach (var file in Directory.GetFiles(dir))
						fileAction(file, relative + Path.GetFileName(file));
				}

				foreach (var directory in Directory.GetDirectories(dir))
					queue.Enqueue(directory);
			}
		}

		public static bool IsFileUnderPath(string filePath, string directory)
		{
			if (string.IsNullOrEmpty(filePath) || string.IsNullOrEmpty(directory))
				return false;

			var fileParts = ExplodePath(filePath);
			var dirParts = ExplodePath(directory);

			if (dirParts.Length > fileParts.Length)
				return false;

			for (int i = 0; i < dirParts.Length; i++)
			{
				if (!string.Equals(dirParts[i], fileParts[i], StringComparison.OrdinalIgnoreCase))
					return false;
			}

			return true;
		}

		public static void DeleteDirectory(string path, Action<string> childPreAction = null)
		{
			if (path == null) throw new ArgumentNullException("path");
			if (!Directory.Exists(path)) throw new DirectoryNotFoundException("Directory doesn't exist!");

			foreach (var file in Directory.GetFiles(path))
			{
				if (childPreAction != null)
					childPreAction(file);

				File.SetAttributes(file, FileAttributes.Normal);
				File.Delete(file);
			}

			foreach (var subDir in Directory.GetDirectories(path))
				DeleteDirectory(subDir, childPreAction);

			Directory.Delete(path);
		}

		public static bool HasExtension(string fileName, string extension)
		{
			return !string.IsNullOrEmpty(fileName)
			       && !string.IsNullOrEmpty(fileName)
			       && fileName.EndsWith(extension, StringComparison.OrdinalIgnoreCase);
		}

		public static string CombineWithAssemblyDirectory(string path)
		{
			var location = Assembly.GetExecutingAssembly().Location;
			if (string.IsNullOrEmpty(location))
				throw new InvalidOperationException("Can't get current assembly location");

			return Path.Combine(Path.GetDirectoryName(location), path);
		}

		public static string GetAssemblyDirectory()
		{
			return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		}

		public static string EnsureSlash(string path, char directorySeparator = '\\')
		{
			if (string.IsNullOrEmpty(path))
				return path;

			if (path[path.Length - 1] == directorySeparator)
				return path;

			if (path[path.Length - 1] == Path.DirectorySeparatorChar || path[path.Length - 1] == Path.AltDirectorySeparatorChar)
				return path.Substring(0, path.Length - 1) + directorySeparator;

			return path + directorySeparator;
		}

		public static string RebasePath(string path, string source, string target, char directorySeparator = '\\')
		{
			return MakeAbsolute(MakeRelative(path, source, directorySeparator), target, directorySeparator);
		}

		public static string MakeRelative(string path, string relativeTo, char directorySeparator = '\\')
		{
			if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(relativeTo))
				return path;

			var pathParts = ExplodePath(path);
			var relativeParts = ExplodePath(relativeTo);
			int cnt;

			for (cnt = 0; cnt < Math.Min(pathParts.Length, relativeParts.Length); cnt++)
				if (!string.Equals(pathParts[cnt], relativeParts[cnt], StringComparison.OrdinalIgnoreCase))
					break;

			if (cnt == 0)
				return path;

			var sb = new StringBuilder();
			for (int i = 0; i < (relativeParts.Length - cnt); i++)
				sb.Append(@"..").Append(directorySeparator);

			for (int i = cnt; i < pathParts.Length; i++)
			{
				sb.Append(pathParts[i]);
				if (i < pathParts.Length - 1)
					sb.Append(directorySeparator);
			}

			return sb.ToString();
		}

		public static string MakeAbsolute(string path, string relativeTo, char directorySeparator = '\\')
		{
			if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(relativeTo))
				return path;

			if (Path.IsPathRooted(path))
				return path;

			var pathParts = ExplodePath(path);
			var relativeParts = ExplodePath(relativeTo);
			int cnt;

			for (cnt = 0; cnt < pathParts.Length; cnt++)
				if (pathParts[cnt] != "..")
					break;

			var sb = new StringBuilder();

			for (int i = 0; i < relativeParts.Length - cnt; i++)
				sb.Append(relativeParts[i]).Append(directorySeparator);

			for (int i = cnt; i < pathParts.Length; i++)
			{
				sb.Append(pathParts[i]);
				if (i < pathParts.Length - 1)
					sb.Append(directorySeparator);
			}

			return sb.ToString();
		}

		private static string[] ExplodePath(string path)
		{
			return path.TrimEnd(_separators).Split(_separators, StringSplitOptions.RemoveEmptyEntries);
		}
	}
}