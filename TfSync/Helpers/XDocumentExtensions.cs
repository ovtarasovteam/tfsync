﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TfSync.Helpers
{
	public static class XDocumentExtensions
    {
        public static XElement ChildElement(this XContainer node, string name)
        {
            return node.Elements().FirstOrDefault(x => x.Name.LocalName == name);
        }

		public static void ChildElement(this XContainer node, string name, Action<XElement> resultAction, Action notFoundAction = null)
		{
			var child = node.Elements().FirstOrDefault(x => x.Name.LocalName == name);
			if (child != null)
				resultAction(child);
			else
			{
				if (notFoundAction != null)
					notFoundAction();
			}
		}

        public static IEnumerable<XElement> ChildElements(this XContainer node, string name)
        {
            return node.Elements().Where(x => x.Name.LocalName == name);
        }

		public static void ChildElements(this XContainer node, string name, Action<XElement> resultAction)
		{
			foreach (var child in node.Elements().Where(x => x.Name.LocalName == name))
				resultAction(child);
		}

        public static XElement DescendantElement(this XContainer node, params string[] names)
        {
            var cur = node;

            for (int i = 0; i < names.Length; i++)
            {
                cur = node.ChildElement(names[i]);
                if (cur == null)
                    return null;
            }

            return cur as XElement;
        }

	    public static void AttributeValue(this XElement element, string name, Action<string> resultAction)
	    {
			var attr = element.Attributes().FirstOrDefault(a => a.Name.LocalName == name);

			if (attr == null)
				return;

		    resultAction(attr.Value);
	    }

        public static string AttributeValue(this XElement element, string name)
        {
            var attr = element.Attributes().FirstOrDefault(a => a.Name.LocalName == name);

            if (attr != null)
                return attr.Value;

            return null;
        }

        public static void AddElement(this XElement element, XElement child)
        {
            element.Add(child);
			child.Name = XName.Get(child.Name.LocalName, element.Name.NamespaceName);
        }

        public static void AddElements(this XElement element, IEnumerable<XElement> children)
        {
            foreach (var child in children)
            {
                element.Add(child);
                element.SetElementsNamespace();
            }
        }

        public static void SetLocalName(this XElement element, string name)
        {
            element.Name = XName.Get(name, element.Name.NamespaceName);
        }

        private static void SetElementsNamespace(this XElement parent)
        {
            foreach (var descendant in parent.Descendants())
                descendant.Name = XName.Get(descendant.Name.LocalName, parent.Name.NamespaceName);
        }
    }
}