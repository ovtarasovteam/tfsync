﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System.Collections.Generic;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace TfSync.Helpers
{
	public static class TfsExtensions
	{
		public static List<Change> GetChangesForChangeset(this VersionControlServer server, int id)
		{
			var result = new List<Change>();

			Change[] page = null;
			while (true)
			{
				var last = page == null ? null : new ItemSpec(page[page.Length - 1].Item.ServerItem, RecursionType.Full);

				page = server.GetChangesForChangeset(id, false, int.MaxValue, last);
				if (page == null || page.Length == 0)
					break;
				
				result.AddRange(page);
			}

			return result;
		}
	}
}