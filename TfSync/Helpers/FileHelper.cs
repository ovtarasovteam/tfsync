﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using TfSync.Changes;
using TfSync.Transformers;

namespace TfSync.Helpers
{
	public static class FileHelper
	{
		public enum CopyDirection
		{
			MasterToSlave,
			SlaveToMaster
		}

		private class FileContent
		{
			public FileContent(string fileName)
			{
				FileName = fileName;
			}

			public string FileName;
			public string Content;
		}

		internal const string TfsSectionExtension = ".tfssection";

		private static readonly Dictionary<string, ITransformer> _transforms;

		static FileHelper()
		{
			var projectTransformer = new ProjectTransformer();

			_transforms = new Dictionary<string, ITransformer>
			              {
				              {".sln", new SolutionTransformer() },
							  {".csproj", projectTransformer},
							  {".vbproj", projectTransformer},
							  {".fsproj", projectTransformer},
							  {".jsproj", projectTransformer},
			              };
		}

		public static List<FileChange> GetFileChanges(HashSet<string> source, HashSet<string> target, string sourceBase, string targetBase)
		{
			var result = new List<FileChange>();
			
			foreach (var file in source)
			{
				string targetPath = PathHelpers.MakeAbsolute(file, targetBase);

				if (!target.Contains(file))
					result.Add(new FileChange(file, ChangeType.Add));
				else
				{
					string sourcePath = PathHelpers.MakeAbsolute(file, sourceBase);
					if (!FilesEqual(sourcePath, targetPath))
						result.Add(new FileChange(file, ChangeType.Modify));
				}
			}

			foreach (var file in target)
			{
				if (!source.Contains(file))
					result.Add(new FileChange(file, ChangeType.Remove));
			}

			return result;
		}

		public static void CopyFiles([NotNull] IEnumerable<string> files, [NotNull] string source, [NotNull] string target, CopyDirection direction)
		{
			if (files == null)
				throw new ArgumentNullException("files");
			if (source == null)
				throw new ArgumentNullException("source");
			if (target == null)
				throw new ArgumentNullException("target");
			if (!Directory.Exists(source))
				throw new InvalidOperationException("Source directory doesn't exist!");

			foreach (var filePath in files)
			{
				var file = new FileInfo(filePath);
				string relativeSource = PathHelpers.MakeRelative(file.DirectoryName, source);
				string absoluteTarget = string.IsNullOrEmpty(relativeSource) ? target : PathHelpers.MakeAbsolute(relativeSource, target);

				if (!Directory.Exists(absoluteTarget))
					Directory.CreateDirectory(absoluteTarget);

				if (!TransformFile(file, direction, absoluteTarget))
				{
					var targetFile = new FileInfo(Path.Combine(absoluteTarget, file.Name));
					if (targetFile.Exists)
						targetFile.IsReadOnly = false;
						
					file.CopyTo(Path.Combine(absoluteTarget, file.Name), true);
					Console.WriteLine("Straight Copy: " + file.Name);
				}
			}
		}

		public static bool TransformFile(FileInfo file, CopyDirection direction, string absoluteTarget)
		{
			ITransformer transform;
			if (!_transforms.TryGetValue(file.Extension.ToLowerInvariant(), out transform)) 
				return false;

			var transformResults = direction == CopyDirection.MasterToSlave
				                       ? StripTfsInfo(file, transform)
				                       : AppendTfsInfo(file, transform);

			foreach (var transformResult in transformResults)
			{
				var targetFile = new FileInfo(Path.Combine(absoluteTarget, transformResult.FileName));
				if (targetFile.Exists)
				{
					if (DiffHelper.StringsEqualIgnoreWhitespace(File.ReadAllText(targetFile.FullName), transformResult.Content))
						continue;

					targetFile.IsReadOnly = false;
				}

				Console.WriteLine("Transform Copy: " + targetFile.Name);
				using (var writer = new StreamWriter(targetFile.FullName))
				{
					writer.Write(transformResult.Content);
				}
			}

			return true;
		}

		private static IEnumerable<FileContent> StripTfsInfo(FileInfo source, ITransformer transformer)
		{
			var result = new List<FileContent>();
			var sourceContent = new FileContent(source.Name);

			result.Add(sourceContent);

			var stripResult = transformer.StripTfsInfo(source.FullName);
			sourceContent.Content = stripResult.Content;

			if (string.IsNullOrEmpty(stripResult.TfsSection))
				return result;

			result.Add(new FileContent(source.Name + TfsSectionExtension)
			           {
				           Content = stripResult.TfsSection
			           });

			return result;
		}

		private static IEnumerable<FileContent> AppendTfsInfo(FileInfo source, ITransformer transformer)
		{
			var solutionContent = new FileContent(source.Name);

			string sectionPath = source.FullName + TfsSectionExtension;
			solutionContent.Content = File.Exists(sectionPath)
				? solutionContent.Content = transformer.AppendTfsInfo(source.FullName, sectionPath)
				: File.ReadAllText(source.FullName);

			return new[] {solutionContent};
		}

		private static bool FilesEqual(string file1, string file2)
		{
			using (var s1 = new FileStream(file1, FileMode.Open, FileAccess.Read))
			using (var s2 = new FileStream(file2, FileMode.Open, FileAccess.Read))
			{
				if (s1.Length != s2.Length)
					return false;

				int b1, b2;
				do
				{
					b1 = s1.ReadByte();
					b2 = s2.ReadByte();
				} while (b1 == b2 && b1 != -1 && b2 != -1);

				return b1 == b2;
			}
		}
	}
}