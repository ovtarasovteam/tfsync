﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.Runtime.Serialization;

namespace TfSync
{
	public class ExitCodeException : Exception
	{
		public ExitCodes ExitCode { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:System.Exception"/> class.
		/// </summary>
		public ExitCodeException(ExitCodes exitCode)
		{
			ExitCode = exitCode;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:System.Exception"/> class with a specified error message.
		/// </summary>
		/// <param name="message">The message that describes the error. </param>
		/// <param name="exitCode">Program exit code</param>
		public ExitCodeException(string message, ExitCodes exitCode) : base(message)
		{
			ExitCode = exitCode;
		}
	}

}