﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System.Globalization;
using CLAP;
using System;
using System.IO;
using System.Linq;

namespace TfSync
{
	/// <summary>
	///      An application which manages the code synchronization between a TFS
	///      and Mercurial repository.
	/// </summary>
	internal class Program
	{
		private static void Main(string[] args)
		{
			ExitCodes status;
			try
			{
				status = (ExitCodes)Parser.Run<Bridge>(args);
#if DEBUG
				Console.WriteLine("Press any key to end debugging...");
				Console.ReadKey(true);
#endif
			}
			catch (VerbNotFoundException error)
			{
                Console.Error.WriteLine(String.Format("Unknown TfSync command: {0}", error.Verb));
				status = ExitCodes.CommandUnknown;
			}
			catch (MissingDefaultVerbException)
			{
                Console.Error.WriteLine("Missing TfSync command.");
				status = ExitCodes.CommandMissing;
			}
			catch (MissingArgumentPrefixException error)
			{
				Console.Error.WriteLine(error.Message);
				status = ExitCodes.CommandArgumentMissing;
			}
			catch (MissingArgumentValueException error)
			{
				Console.Error.WriteLine(error.Message);
				status = ExitCodes.CommandArgumentMissing;
			}
			catch (MissingRequiredArgumentException error)
			{
				Console.Error.WriteLine(error.Message);
				status = ExitCodes.CommandArgumentRequiredMissing;
			}
			catch (FileNotFoundException error)
			{
				if (error.Message.Contains(
						"Could not load file or assembly 'Microsoft.TeamFoundation"))
				{
					Console.Error.WriteLine(string.Format(
							"Failed to load version {0} of TFS.",
							typeof(Program
							).Assembly.GetReferencedAssemblies().Where
							(
								asm =>
								asm.Name == "Microsoft.TeamFoundation.Client"
							).Single().Version.Major));
					status = ExitCodes.MissingTFS;
				}
				else
					throw;
			}
			catch (TypeInitializationException)
			{
				Console.Error.WriteLine(string.Format(
						"Failed to load version {0} of TFS.",
						typeof(Program
						).Assembly.GetReferencedAssemblies().Where
						(
							asm =>
							asm.Name == "Microsoft.TeamFoundation.Client"
						).Single().Version.Major));
				status = ExitCodes.MissingTFS;
			}
			catch (InvalidOperationException error)
			{
				Console.Error.WriteLine("\n\nCommand error: {0}", error.Message);
				status = ExitCodes.CommandExecutionFailure;
			}
			catch (ExitCodeException e)
			{
				Console.WriteLine(e.Message);
				status = e.ExitCode;
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(String.Format("\n\n\t\t\tFatal error\nType:\t{0}\nMessage:\t{1}\nCall Stack:\n{2}", e.GetType(), e.Message, e.StackTrace));
				status = ExitCodes.Unknown;
			}

			Environment.ExitCode = (int)status;
		}
	}
}