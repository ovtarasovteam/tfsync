﻿/*******************************************************************************
***  This file is part of TfSync.                                             ***
***                                                                          ***
***  Copyright (C) 2013 - 2015  NotLimited                                   ***
***  Copyright (C) 2013 - 2015  John Schneiderman                            ***
***                                                                          ***
***  This program is free software: you can redistribute it and/or modify    ***
***  it under the terms of the GNU General Public License as published by    ***
***  the Free Software Foundation, either version 3 of the License, or       ***
***  (at your option) any later version.                                     ***
***                                                                          ***
***  This program is distributed in the hope that it will be useful, but     ***
***  WITHOUT ANY WARRANTY; without even the implied warranty of              ***
***  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    ***
***  See the GNU General Public License for more details.                    ***
***                                                                          ***
***  You should have received a copy of the GNU General Public License       ***
***  along with this program. If not, see <http://www.gnu.org/licenses/>.    ***
*******************************************************************************/
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using CLAP;
using CLAP.Interception;
using JetBrains.Annotations;
using TfSync.Changes;
using TfSync.Helpers;
using TfSync.SourceControl;

namespace TfSync
{
	public class Bridge
	{
		[Verb(Description = HelpText.CloneDescription)]
		public static void Clone(
			[NotNull, Required, Description(HelpText.CloneMasterDescription), Aliases("m")]	string master,
			[Description(HelpText.CloneRepoDecription), Aliases("R")]						string repo = null)
		{
			string masterName = Path.GetDirectoryName(master);
			if (string.IsNullOrEmpty(masterName))
				throw new InvalidOperationException("Can't extract master repository name.");

			if (string.IsNullOrEmpty(repo))
				repo = Path.Combine(Environment.CurrentDirectory, masterName);

			HgRepository.VerifyCloneTarget(repo);
			
			Console.WriteLine("Checking the TFS master for pending changes.");
			var tfsRepo = new TfsRepository(master);
			if (tfsRepo.HasPendingChanges)
				throw new InvalidOperationException("Cannot clone while the TFS master has pending changes.");
            // Ensure the master repository is at the latest changes.
            tfsRepo.Update();

			Console.WriteLine("Requesting the tracked files from the TFS master.");
			FileHelper.CopyFiles(tfsRepo.GetItemsUnderControl(), master, repo, FileHelper.CopyDirection.MasterToSlave);

			Console.WriteLine("\nCreating the Hg slave repository clone using the TFS master.");
			HgRepository.Create(repo, master, tfsRepo.LastChangeSetId);

			Console.WriteLine("Clone operation finished.");
		}

		[Verb(Description = HelpText.PushDescription)]
		public static void Push(
			[Description(HelpText.CloneRepoDecription), Aliases("R")]		string repo,
			[Description(HelpText.PushCommentDecription), Aliases("c")]		string comment,
			[Description(HelpText.PushWorkitemsDescription), Aliases("w")]	int[] workItems)
		{
			if (string.IsNullOrEmpty(repo))
				repo = Environment.CurrentDirectory;

			Console.WriteLine("Checking the Hg slave for uncommited changes.");
			var repository = HgRepository.Open(repo);
			if (repository.HasUncommitedChanges)
				throw new InvalidOperationException("Cannot push while the Hg slave has uncommited changes.");

			var tfsRepository = new TfsRepository(repository.MasterPath);

			if (tfsRepository.LastChangeSetId != repository.LastMasterChangeset)
			{
				Console.WriteLine("Pulling changes from TFS master.");
				Pull(repo);
			}
			else
			   Console.WriteLine("The TFS master does not have any new changes.");


			Console.WriteLine("Collecting the Hg slave change-sets to push.");
			var changes = repository.GetChangesToPush();
			if (changes == null || changes.Count == 0)
				throw new ExitCodeException("Hg slave does not have any change-sets to push.", ExitCodes.NothingToPush);

			Console.WriteLine("Pushing Hg slave files.");
			var modified = changes.Where(x => x.Type == ChangeType.Modify || x.Type == ChangeType.Add).Select(x => PathHelpers.MakeAbsolute(x.Path, repo)).ToList();

			if (modified.Count > 0)
				FileHelper.CopyFiles(modified, repo, repository.MasterPath, FileHelper.CopyDirection.SlaveToMaster);

			Console.WriteLine("\nPropagating changes to TFS master.");
			var targetChanges = changes.Select(x => new FileChange(PathHelpers.MakeAbsolute(x.Path, repository.MasterPath), x.Type)).ToList();
			tfsRepository.PropagateChanges(targetChanges, repository.GetFilesUnderControl(true), repo);

			TfsCheckIn(tfsRepository, repository, comment, workItems);

			Console.WriteLine("Push operation finished.");
		}

		private static void TfsCheckIn(TfsRepository tfsRepository, HgRepository repository, string comment, int[] workItems)
		{
			Console.WriteLine("Checking-in changes from Hg slave into TFS master repository.");
			int changeset;
			try
			{
				changeset = tfsRepository.CheckIn(string.IsNullOrEmpty(comment) ? repository.GetDefaultComment() : comment, workItems);
			}
			catch (Exception e)
			{
				Error(e.Message);
				Console.WriteLine("Rolling changes back in TFS master repository.");
				tfsRepository.Rollback();

				return;
			}

			repository.ConfirmPush(changeset);
		}

		[Verb(Description = HelpText.PullDescription)]
		public static void Pull([Description(HelpText.CloneRepoDecription), Aliases("R")] string repo)
		{
			if (string.IsNullOrEmpty(repo))
				repo = Environment.CurrentDirectory;

			Console.WriteLine("Checking the Hg slave for uncommited changes.");
			var repository = HgRepository.Open(repo);
			if (repository.HasUncommitedChanges)
			   throw new InvalidOperationException("Cannot pull while the Hg slave has uncommited changes.");


			Console.WriteLine("Checking the TFS master for changes to pull.");
			var tfsRepository = new TfsRepository(repository.MasterPath);
			if (tfsRepository.LastChangeSetId == repository.LastMasterChangeset)
			   throw new ExitCodeException("TFS master does not have any change-sets to pull.", ExitCodes.NothingToPull);

			Console.WriteLine("Updating TFS master repository to the latest version.");
			tfsRepository.Update();

			Console.WriteLine("Collecting the TFS master change-sets to pull.");
			var tfsChanges = tfsRepository.GetChanges(repository.LastMasterChangeset);
			if (tfsChanges == null || tfsChanges.Changes.Count == 0)
			   throw new ExitCodeException("TFS master does not have any change-sets to pull.", ExitCodes.NothingToPull);

#if !DEBUG
			try
			{
#endif
				if (repository.LastRevision == repository.CurrentRevision)
				{
					ApplyChanges(tfsChanges, repository);

					Console.WriteLine("\nCommiting TFS master changes to Hg slave.");
					repository.CommitPulled(tfsChanges.ChangesetFrom, tfsChanges.ChangeSetTo);
				}
				else
				{
					var branchHead = repository.UpdateToLastRevision();

					ApplyChanges(tfsChanges, repository);

					Console.WriteLine("\nCommiting TFS master changes to Hg slave.");
					repository.MergePulled(branchHead, tfsChanges.ChangesetFrom, tfsChanges.ChangeSetTo);
				}
#if !DEBUG
			}
			catch (Exception e)
			{
				throw new InvalidOperationException("WARNING! The Hg slave repository is in an unknown state. This must be fixed manually.", e);
			}
#endif

			Console.WriteLine("Pull operation finished.");
		}

		[Verb(Description = HelpText.ExposeDescription)]
		public static void Expose([Description(HelpText.CloneRepoDecription), Aliases("R")] string repo)
		{
			if (string.IsNullOrEmpty(repo))
				repo = Environment.CurrentDirectory;

			PathHelpers.IterateDirectories(repo, null, (path, relativePath) =>
			                                           {
				                                           var fi = new FileInfo(path);
				                                           if (fi.Exists && fi.IsReadOnly)
				                                           {
					                                           Console.WriteLine("Removed read-only flag from: " + relativePath);
					                                           fi.IsReadOnly = false;
				                                           }
			                                           });

			Console.WriteLine("Read-only flag removal operation finished.");
		}

		[Verb(Description = HelpText.LinkDescription)]
		public static void Link(
			[Description(HelpText.CloneRepoDecription), Aliases("R")]						string repo,
			[NotNull, Description(HelpText.CloneMasterDescription), Required, Aliases("m")]	string master,
			[Description(HelpText.PushWorkitemsDescription), Aliases("w")]					int[] workItems,
			[Description(HelpText.PriorityDescription), Aliases("p")]						Priority priority = Priority.Equal)
		{
			if (string.IsNullOrEmpty(master)) 
				throw new ArgumentNullException("master");
			if (string.IsNullOrEmpty(repo))
				repo = Environment.CurrentDirectory;

			if (!Directory.Exists(master))
				throw new DirectoryNotFoundException("The TFS repository was not found.");
			
			Console.WriteLine("Checking for pending changes in the TFS repository.");
			var tfsRepo = new TfsRepository(master);
			if (tfsRepo.HasPendingChanges)
			   throw new InvalidOperationException("Cannot link while the TFS master has uncommited changes.");

			var hgRepo = HgRepository.Open(repo);
			if (hgRepo.HasUncommitedChanges)
				throw new InvalidOperationException("Cannot link while Mercurial slave has uncommited changes.");

			Console.WriteLine("Linking the TFS master to the Hg slave. Priority is " + priority.ToString());
			
			hgRepo.Link(repo, master, tfsRepo.LastChangeSetId);
			if (priority != Priority.Equal)
			{
				var tfsFiles = tfsRepo.GetItemsUnderControl(true);
				var hgFiles = hgRepo.GetFilesUnderControl(true);
				var source = priority == Priority.Slave ? hgFiles : tfsFiles;
				var target = priority == Priority.Slave ? tfsFiles : hgFiles;
				string sourceBase = priority == Priority.Slave ? repo : master;
				string targetBase = priority == Priority.Slave ? master : repo;

				var changes = FileHelper.GetFileChanges(source, target, sourceBase, targetBase);
				var modified = changes.Where(x => x.Type == ChangeType.Add || x.Type == ChangeType.Modify).Select(x => PathHelpers.MakeAbsolute(x.Path, sourceBase)).ToList();
				if (modified.Count > 0)
					FileHelper.CopyFiles(modified, sourceBase, targetBase, priority == Priority.Slave ? FileHelper.CopyDirection.SlaveToMaster : FileHelper.CopyDirection.MasterToSlave);

				changes = changes.Select(x => new FileChange(PathHelpers.MakeAbsolute(x.Path, targetBase), x.Type)).ToList();
				if (priority == Priority.Slave)
				{
					tfsRepo.PropagateChanges(changes, hgFiles, repo);
					TfsCheckIn(tfsRepo, hgRepo, "Linked with Mercurial repository", workItems);
				}
				else
				{
					foreach (var removed in changes.Where(x => x.Type == ChangeType.Remove))
						File.Delete(removed.Path);
					hgRepo.CommitPulled(0, 0);
				}
			}

			Console.WriteLine("Link operation finished.");
		}

		private static void ApplyChanges(TfsChanges tfsChanges, HgRepository repository)
		{
		   Console.WriteLine("Applying pulled code changes from TFS master.");
			var modified = tfsChanges.Changes.Where(x => x.Type == ChangeType.Modify || x.Type == ChangeType.Add).Select(x => x.Path).ToList();
			if (modified.Count > 0)
				FileHelper.CopyFiles(modified, repository.MasterPath, repository.RepositoryPath, FileHelper.CopyDirection.MasterToSlave);

			foreach (var removed in tfsChanges.Changes.Where(x => x.Type == ChangeType.Remove))
				File.Delete(PathHelpers.RebasePath(removed.Path, repository.MasterPath, repository.RepositoryPath));
		}

		[PreVerbExecution]
		private static void Before(PreVerbExecutionContext context)
		{
			Console.WriteLine("TfSync version " + Assembly.GetExecutingAssembly().GetName().Version + '\n');
		}

		[Help, Empty]
		public static void Help(string help)
		{
			Before(null);
			Console.WriteLine("   tfsync command [verb-params]");
			Console.WriteLine("\nCommands:");
			Console.WriteLine(help);
			Console.WriteLine("\nThe current executing directory is used\n\tfor any omited path that is not required.");
		}

	   /// <summary>
	   /// Displays an error message in the error buffer.
	   /// </summary>
	   /// <param name="message">The error message to display.</param>
		private static void Error(string message)
		{
			Console.Error.WriteLine(message);
		}
	}
}